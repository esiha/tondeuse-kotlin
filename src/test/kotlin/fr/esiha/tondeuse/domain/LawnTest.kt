package fr.esiha.tondeuse.domain

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource

internal class LawnTest {
    @ParameterizedTest
    @CsvSource("0, 0", "5, 5", "3, 3")
    internal fun `should tell when lawn contains coordinates`(abscissa: Int, ordinate: Int) {
        assertThat(Coordinates(abscissa, ordinate) in Lawn.rectangular(Coordinates(5, 5))).isTrue
    }

    @ParameterizedTest
    @CsvSource("-1, 0", "5, 6")
    internal fun `should tell when lawn does not contain coordinates`(abscissa: Int, ordinate: Int) {
        assertThat(Coordinates(abscissa, ordinate) in Lawn.rectangular(Coordinates(5, 5))).isFalse
    }
}