package fr.esiha.tondeuse.domain

import fr.esiha.tondeuse.domain.Orientation.EAST
import fr.esiha.tondeuse.domain.Orientation.NORTH
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory

class PositionTest {
    @Test
    internal fun `should turn orientation by a left quarter turn`() {
        val position = Position(Coordinates(3, 4), NORTH)
        assertThat(position.quarterTurnLeft()).isEqualTo(position.copy(orientation = NORTH.quarterTurnLeft()))
    }

    @Test
    internal fun `should turn orientation by a right quarter turn`() {
        val position = Position(Coordinates(3, 4), NORTH)
        assertThat(position.quarterTurnRight()).isEqualTo(position.copy(orientation = NORTH.quarterTurnRight()))
    }

    @TestFactory
    internal fun `should step by one in the corresponding direction`() = Orientation.values()
            .map {
                dynamicTest("$it") {
                    val coordinates = Coordinates(3, 4)
                    val position = Position(coordinates, it)
                    assertThat(position + 1)
                            .isEqualTo(position.copy(coordinates = it.translateCoordinates(coordinates, 1)))
                }
            }

    @Test
    internal fun `should wrap abscissa and ordinate in coordinates`() {
        assertThat(Position(4, 9, EAST))
                .isEqualTo(Position(Coordinates(4, 9), EAST))
    }
}