package fr.esiha.tondeuse.domain

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class CoordinatesTest {
    @Test
    internal fun `should add abscissa`() {
        assertThat(Coordinates(3, 5).plusAbscissa(1))
                .matches { it.abscissa == 4 }
                .isEqualTo(Coordinates(4, 5))
    }

    @Test
    internal fun `should add ordinate`() {
        assertThat(Coordinates(3, 5).plusOrdinate(1))
                .isEqualTo(Coordinates(3, 6))
    }

    @Test
    internal fun `should tell when in upper right quadrant`() {
        assertThat(Coordinates(3, 4) isInUpperRightQuadrantOf Coordinates(0, 0))
                .isTrue
        Coordinates(3, 4).isInUpperRightQuadrantOf(Coordinates(0, 0))
    }
}