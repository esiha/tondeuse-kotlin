package fr.esiha.tondeuse.domain

import fr.esiha.tondeuse.domain.Instruction.*
import fr.esiha.tondeuse.domain.Orientation.EAST
import fr.esiha.tondeuse.domain.Orientation.NORTH
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatIllegalArgumentException
import org.junit.jupiter.api.Test

class MowerRunnerTest {
    @Test
    internal fun `should fail to create with Mower initial Position outside of Lawn`() {
        with(MowerRunner(listOf(createMower(-1, -1)),
                Lawn.rectangular(Coordinates(3, 3)))) {
            assertThatIllegalArgumentException().isThrownBy { mow() }
        }
    }

    @Test
    internal fun `should run mowers on the provided lawn`() {
        with(MowerRunner(listOf(
                createMower(1, 2, NORTH, listOf(TURN_LEFT, MOVE_ONCE, TURN_LEFT, MOVE_ONCE, TURN_LEFT, MOVE_ONCE, TURN_LEFT, MOVE_ONCE, MOVE_ONCE)),
                createMower(3, 3, EAST, listOf(MOVE_ONCE, MOVE_ONCE, TURN_RIGHT, MOVE_ONCE, MOVE_ONCE, TURN_RIGHT, MOVE_ONCE, TURN_RIGHT, TURN_RIGHT, MOVE_ONCE))),
                Lawn.rectangular(Coordinates(5, 5))
        )) {
            mow()
            assertThat(currentMowersPositions())
                    .isEqualTo(listOf(Position(1, 3, NORTH), Position(5, 1, EAST)))
        }
    }

    private fun createMower(abscissa: Int, ordinate: Int, orientation: Orientation = NORTH, instructions: List<Instruction> = emptyList()) =
            Mower(Position(abscissa, ordinate, orientation), instructions)
}