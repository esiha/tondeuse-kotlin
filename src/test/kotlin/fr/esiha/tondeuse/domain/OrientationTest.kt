package fr.esiha.tondeuse.domain

import fr.esiha.tondeuse.domain.Orientation.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource

class OrientationTest {
    @Test
    internal fun `should declare all known values`() {
        assertThat(values())
                .containsExactlyInAnyOrder(NORTH, SOUTH, EAST, WEST)
    }

    @ParameterizedTest
    @CsvSource(
            "NORTH, WEST",
            "WEST, SOUTH",
            "SOUTH, EAST",
            "EAST, NORTH"
    )
    internal fun `should perform quarter turn to the left`(start: Orientation, expected: Orientation) {
        assertThat(start.quarterTurnLeft()).isEqualTo(expected)
    }

    @ParameterizedTest
    @CsvSource(
            "NORTH, EAST",
            "EAST, SOUTH",
            "SOUTH, WEST",
            "WEST, NORTH"
    )
    internal fun `should perform quarter turn to the right`(start: Orientation, expected: Orientation) {
        assertThat(start.quarterTurnRight()).isEqualTo(expected)
    }

    @Nested
    inner class TranslateCoordinates {
        @Test
        internal fun `should add ordinate when translating NORTH`() {
            assertThat(NORTH.translateCoordinates(Coordinates(3, 4), 1))
                    .isEqualTo(Coordinates(3, 5))
        }

        @Test
        internal fun `should add abscissa when translating EAST`() {
            assertThat(EAST.translateCoordinates(Coordinates(3, 4), 1))
                    .isEqualTo(Coordinates(4, 4))
        }

        @Test
        internal fun `should subtract ordinate when translating SOUTH`() {
            assertThat(SOUTH.translateCoordinates(Coordinates(3, 4), 1))
                    .isEqualTo(Coordinates(3, 3))
        }

        @Test
        internal fun `should subtract abscissa when translating WEST`() {
            assertThat(WEST.translateCoordinates(Coordinates(3, 4), 1))
                    .isEqualTo(Coordinates(2, 4))
        }
    }
}