package fr.esiha.tondeuse.domain

import fr.esiha.tondeuse.domain.Instruction.*
import fr.esiha.tondeuse.domain.Orientation.NORTH
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class InstructionTest {
    @Test
    internal fun `should define all known instructions`() {
        assertThat(Instruction.values())
                .containsExactlyInAnyOrder(MOVE_ONCE, TURN_LEFT, TURN_RIGHT)
    }

    @Test
    internal fun `should turn position by a quarter turn left when turning left`() {
        val position = Position(3, 4, NORTH)
        assertThat(TURN_LEFT.execute(position))
                .isEqualTo(position.quarterTurnLeft())
    }

    @Test
    internal fun `should turn position by a quarter turn right when turning right`() {
        val position = Position(3, 4, NORTH)
        assertThat(TURN_RIGHT.execute(position))
                .isEqualTo(position.quarterTurnRight())
    }

    @Test
    internal fun `should add one to position when moving once`() {
        val position = Position(3, 4, NORTH)
        assertThat(MOVE_ONCE.execute(position))
                .isEqualTo(position + 1)
    }
}