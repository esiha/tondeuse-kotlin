package fr.esiha.tondeuse.domain

import fr.esiha.tondeuse.domain.Instruction.*
import fr.esiha.tondeuse.domain.Orientation.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class MowerTest {
    @Test
    internal fun `should not move when instructions are empty`() {
        val position = Position(1, 2, NORTH)
        val mower = Mower(position, emptyList())

        mower.mow(fiveByFiveRectangularLawn())

        assertThat(mower.currentPosition).isEqualTo(position)
    }

    @Test
    internal fun `should execute instructions in the given order`() {
        val position = Position(1, 2, NORTH)
        val instructions = listOf(MOVE_ONCE, TURN_RIGHT)
        val mower = Mower(position, instructions)

        mower.mow(fiveByFiveRectangularLawn())

        assertThat(mower.currentPosition)
                .isEqualTo(instructions.fold(position) { current, instruction -> instruction.execute(current) })
    }

    @Test
    internal fun `should remain within lawn bounds`() {
        val position = Position(0, 0, EAST)
        val mower = Mower(position, listOf(TURN_RIGHT, MOVE_ONCE))

        mower.mow(fiveByFiveRectangularLawn())

        assertThat(mower.currentPosition).isEqualTo(TURN_RIGHT.execute(position))
    }

    private fun fiveByFiveRectangularLawn() = Lawn.rectangular(Coordinates(5, 5))
}