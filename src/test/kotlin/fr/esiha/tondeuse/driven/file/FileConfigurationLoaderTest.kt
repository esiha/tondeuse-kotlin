package fr.esiha.tondeuse.driven.file

import fr.esiha.tondeuse.domain.*
import fr.esiha.tondeuse.domain.Instruction.*
import fr.esiha.tondeuse.domain.Orientation.EAST
import fr.esiha.tondeuse.domain.Orientation.NORTH
import fr.esiha.tondeuse.driven.file.FileConfigurationLoader.NonexistentFileException
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.assertj.core.api.SoftAssertions.assertSoftly
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import java.io.File
import java.nio.file.Path

class FileConfigurationLoaderTest {
    @Nested
    inner class FileConfigurationLoaderTest {
        @Test
        internal fun `should be a ConfigurationLoader`() {
            assertThat(ConfigurationLoader::class.java).isAssignableFrom(FileConfigurationLoader::class.java)
        }

        @Test
        internal fun `should fail to load configuration from non existent file`() {
            with(FileConfigurationLoader(file("/this file should not exist"))) {
                assertThatExceptionOfType(NonexistentFileException::class.java)
                        .isThrownBy { loadConfiguration() }
                        .withMessageContaining(configurationFile.name)
            }
        }

        @Test
        internal fun `should load configuration from valid file`() {
            with(FileConfigurationLoader(resourceFile("valid")).loadConfiguration()) {
                assertSoftly {
                    it.assertThat(lawn).isEqualTo(Lawn.rectangular(Coordinates(5, 5)))
                    it.assertThat(mowers).hasSize(2)
                    with(mowers[0]) {
                        it.assertThat(initialPosition).isEqualTo(Position(1, 2, NORTH))
                        it.assertThat(instructions).containsExactly(TURN_LEFT, MOVE_ONCE, TURN_LEFT, MOVE_ONCE, TURN_LEFT, MOVE_ONCE, TURN_LEFT, MOVE_ONCE, MOVE_ONCE)
                    }
                    with(mowers[1]) {
                        it.assertThat(initialPosition).isEqualTo(Position(3, 3, EAST))
                        it.assertThat(instructions).containsExactly(MOVE_ONCE, MOVE_ONCE, TURN_RIGHT, MOVE_ONCE, MOVE_ONCE, TURN_RIGHT, MOVE_ONCE, TURN_RIGHT, TURN_RIGHT, MOVE_ONCE)
                    }
                }
            }
        }
    }

    private companion object {
        private const val PATH = "/fr.esiha.tondeuse.driven.file/"
        private fun file(path: String): File = Path.of(path).toFile()
        private fun resourceFile(resourceName: String): File = file(this::class.java.getResource(PATH + resourceName)!!.path)
    }

    @Nested
    inner class InvalidConfigurationFileExceptionTest {
        @Test
        internal fun `should create NonexistentFileException with readable message`() {
            with("/path") {
                assertThat(FileConfigurationLoader.NonexistentFileException(File(this)))
                        .hasMessage("The file '$this' does not exist.")
            }
        }
    }
}