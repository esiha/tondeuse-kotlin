package fr.esiha.tondeuse.driven.file

import fr.esiha.tondeuse.domain.Instruction.*
import fr.esiha.tondeuse.driven.file.MowerInstructionsParser.InvalidMowerInstructionsDefinitionException
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import org.junit.jupiter.params.provider.ValueSource

class MowerInstructionsParserTest {
    @Nested
    inner class MowerInstructionsParserTest {
        @ParameterizedTest
        @ValueSource(strings = ["ADX", "X", "A D G"])
        internal fun `should fail to parse invalid definitions`(input: String) {
            with(MowerInstructionsParser(input)) {
                assertThatExceptionOfType(InvalidMowerInstructionsDefinitionException::class.java)
                        .isThrownBy { parseMowerInstructions() }
                        .withMessageContaining(input)
            }
        }

        @ParameterizedTest
        @CsvSource("ADG", " ADG", "ADG ")
        internal fun `should parse valid definitions`(input: String) {
            with(MowerInstructionsParser(input)) {
                assertThat(parseMowerInstructions())
                        .isEqualTo(listOf(MOVE_ONCE, TURN_RIGHT, TURN_LEFT))
            }
        }
    }

    @Nested
    inner class InvalidMowerInstructionsDefinitionExceptionTest {
        @Test
        internal fun `should be an InvalidConfigurationFileException`() {
            assertThat(InvalidConfigurationFileException::class.java)
                    .isAssignableFrom(InvalidMowerInstructionsDefinitionException::class.java)
        }

        @Test
        internal fun `should have a readable message`() {
            assertThat(InvalidMowerInstructionsDefinitionException("X"))
                    .hasMessageStartingWith("Mower Instructions 'X' does not conform to regular expression '")
        }
    }
}