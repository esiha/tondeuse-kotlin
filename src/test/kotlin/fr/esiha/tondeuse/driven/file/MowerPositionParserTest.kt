package fr.esiha.tondeuse.driven.file

import fr.esiha.tondeuse.domain.Orientation
import fr.esiha.tondeuse.domain.Orientation.NORTH
import fr.esiha.tondeuse.domain.Position
import fr.esiha.tondeuse.driven.file.MowerPositionParser.InvalidMowerPositionDefinitionException
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import org.junit.jupiter.params.provider.ValueSource

internal class MowerPositionParserTest {
    @Nested
    internal inner class MowerPositionParserTest {
        @ParameterizedTest
        @ValueSource(strings = ["X 5 N", "5 X N", "5 5 X"])
        internal fun `should fail to parse invalid mower position definitions`(input: String) {
            with(MowerPositionParser(input)) {
                assertThatExceptionOfType(InvalidMowerPositionDefinitionException::class.java)
                        .isThrownBy { parseMowerPosition() }
                        .withMessageContaining(input)
            }
        }

        @ParameterizedTest
        @ValueSource(strings = ["3 5 N", " 3 5 N", "3  5 N", "3 5  N", "3 5 N "])
        internal fun `should parse valid mower position definitions`(input: String) {
            with(MowerPositionParser(input)) {
                assertThat(parseMowerPosition())
                        .isEqualTo(Position(3, 5, NORTH))
            }
        }

        @ParameterizedTest
        @CsvSource("N, NORTH", "E, EAST", "W, WEST", "S, SOUTH")
        internal fun `should parse all known orientations`(letter: String, orientation: Orientation) {
            with(MowerPositionParser("1 2 $letter")) {
                assertThat(parseMowerPosition())
                        .isEqualTo(Position(1, 2, orientation))
            }
        }
    }

    @Nested
    internal inner class InvalidMowerPositionDefinitionExceptionTest {
        @Test
        internal fun `should be an InvalidConfigurationFileException`() {
            assertThat(InvalidConfigurationFileException::class.java)
                    .isAssignableFrom(InvalidMowerPositionDefinitionException::class.java)
        }

        @Test
        internal fun `should create with readable message`() {
            assertThat(InvalidMowerPositionDefinitionException("5 5 X"))
                    .hasMessageStartingWith("Mower Position '5 5 X' does not conform to regular expression '")
        }
    }
}