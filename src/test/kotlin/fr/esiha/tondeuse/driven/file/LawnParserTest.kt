package fr.esiha.tondeuse.driven.file

import fr.esiha.tondeuse.domain.Coordinates
import fr.esiha.tondeuse.domain.Lawn
import fr.esiha.tondeuse.driven.file.LawnParser.InvalidLawnDefinitionException
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource

internal class LawnParserTest {
    @Nested
    inner class LawnParserTest {
        @ParameterizedTest
        @ValueSource(strings = ["X 5", "5 X", "5 5 5"])
        internal fun `should fail to parse invalid definitions`(input: String) {
            with(LawnParser(input)) {
                assertThatExceptionOfType(InvalidLawnDefinitionException::class.java)
                        .isThrownBy { parseLawn() }
                        .withMessageContaining(input)
            }
        }

        @ParameterizedTest
        @ValueSource(strings = ["3 4", " 3 4", "3  4", "3 4 "])
        internal fun `should parse valid definitions`(input: String) {
            with(LawnParser(input)) {
                assertThat(parseLawn()).isEqualTo(Lawn.rectangular(Coordinates(3, 4)))
            }
        }
    }

    @Nested
    inner class InvalidLawnDefinitionExceptionTest {
        @Test
        internal fun `should be an InvalidConfigurationFileException`() {
            assertThat(InvalidConfigurationFileException::class.java)
                    .isAssignableFrom(InvalidLawnDefinitionException::class.java)
        }

        @Test
        internal fun `should have readable message`() {
            assertThat(InvalidLawnDefinitionException("X 5"))
                    .hasMessageStartingWith("Lawn definition 'X 5' does not conform to regular expression '")
        }
    }
}