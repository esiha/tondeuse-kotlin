package fr.esiha.tondeuse.driven.file

import org.assertj.core.api.SoftAssertions.assertSoftly
import org.junit.jupiter.api.Test

internal class MowerParserTest {
    @Test
    internal fun `should use provided position and instructions parsers`() {
        val positionParser = MowerPositionParser("1 3 E")
        val instructionsParser = MowerInstructionsParser("AADDGAAG")

        with(MowerParser(positionParser, instructionsParser).parseMower()) {
            assertSoftly {
                it.assertThat(initialPosition).isEqualTo(positionParser.parseMowerPosition())
                it.assertThat(instructions).isEqualTo(instructionsParser.parseMowerInstructions())
            }
        }
    }
}