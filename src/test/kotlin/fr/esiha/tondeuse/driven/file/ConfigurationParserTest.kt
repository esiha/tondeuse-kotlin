package fr.esiha.tondeuse.driven.file

import org.assertj.core.api.SoftAssertions.assertSoftly
import org.junit.jupiter.api.Test

internal class ConfigurationParserTest {
    @Test
    internal fun `should use provided lawn and mowers parsers`() {
        val lawnParser = LawnParser("5 5")
        val mowerParsers = listOf(
                MowerParser(MowerPositionParser("1 2 N"), MowerInstructionsParser("AD")),
                MowerParser(MowerPositionParser("3 3 E"), MowerInstructionsParser("DGDG"))
        )
        with(ConfigurationParser(lawnParser, mowerParsers).parseConfiguration()) {
            assertSoftly {
                it.assertThat(lawn).isEqualTo(lawnParser.parseLawn())
                it.assertThat(mowers).hasSize(2)
            }
        }
    }
}