package fr.esiha.tondeuse.application

import fr.esiha.tondeuse.domain.ConfigurationLoader
import fr.esiha.tondeuse.domain.MowerRunner
import fr.esiha.tondeuse.domain.MowingService
import fr.esiha.tondeuse.driven.file.FileConfigurationLoader
import java.io.File

class DependenciesFactory private constructor() {
    companion object {
        fun fileConfigurationLoader(configurationFile: String): ConfigurationLoader = FileConfigurationLoader(File(configurationFile))
        fun mowingService(configurationLoader: ConfigurationLoader): MowingService = MowerRunner(configurationLoader)
    }
}