package fr.esiha.tondeuse.driven.file

import fr.esiha.tondeuse.domain.Instruction.*
import fr.esiha.tondeuse.driven.file.MowerInstructionsParser.Instruction.Companion.toInstruction
import fr.esiha.tondeuse.domain.Instruction as DomainInstruction
import java.util.regex.Pattern

data class MowerInstructionsParser(private val input: String) {
    fun parseMowerInstructions(): List<DomainInstruction> = with(INPUT_PATTERN.matcher(input)) {
        if (matches()) {
            return group(INSTRUCTIONS_GROUP).map { it.toInstruction() }
        }
        throw InvalidMowerInstructionsDefinitionException(input)
    }

    private companion object {
        private val INPUT_PATTERN = Pattern.compile("\\s*(${Instruction.REGEX}+)\\s*")
        private const val INSTRUCTIONS_GROUP = 1
    }

    private enum class Instruction(val domainValue: DomainInstruction) {
        A(MOVE_ONCE), D(TURN_RIGHT), G(TURN_LEFT);

        companion object {
            val REGEX = values().joinToString(prefix = "[", separator = "", postfix = "]") { it.name }
            fun Char.toInstruction(): DomainInstruction = valueOf(toString()).domainValue
        }
    }

    internal data class InvalidMowerInstructionsDefinitionException(private val input: String) :
            InvalidConfigurationFileException("Mower Instructions '$input' does not conform to regular expression '$INPUT_PATTERN'.")
}