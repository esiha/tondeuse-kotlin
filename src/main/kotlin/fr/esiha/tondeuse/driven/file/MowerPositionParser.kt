package fr.esiha.tondeuse.driven.file

import fr.esiha.tondeuse.domain.Orientation.*
import fr.esiha.tondeuse.domain.Position
import fr.esiha.tondeuse.driven.file.MowerPositionParser.Orientation.Companion.ORIENTATION_REGEX
import fr.esiha.tondeuse.driven.file.MowerPositionParser.Orientation.Companion.toDomainOrientation
import java.util.regex.Pattern
import fr.esiha.tondeuse.domain.Orientation as DomainOrientation

data class MowerPositionParser(private val input: String) {
    fun parseMowerPosition(): Position = with(POSITION_PATTERN.matcher(input)) {
        if (matches()) {
            return Position(
                    group(ABSCISSA_GROUP).toInt(),
                    group(ORDINATE_GROUP).toInt(),
                    group(ORIENTATION_GROUP).toDomainOrientation())
        }
        throw InvalidMowerPositionDefinitionException(input)
    }

    private companion object {
        private val POSITION_PATTERN = Pattern.compile("\\s*(\\d+)\\s+(\\d+)\\s+($ORIENTATION_REGEX)\\s*")
        private const val ABSCISSA_GROUP = 1
        private const val ORDINATE_GROUP = 2
        private const val ORIENTATION_GROUP = 3
    }

    private enum class Orientation(val domainValue: DomainOrientation) {
        N(NORTH), E(EAST), W(WEST), S(SOUTH);

        companion object {
            val ORIENTATION_REGEX = values().joinToString(prefix = "[", separator = "", postfix = "]") { it.name }
            fun String.toDomainOrientation(): DomainOrientation = valueOf(this).domainValue
        }
    }

    internal data class InvalidMowerPositionDefinitionException(private val input: String) :
            InvalidConfigurationFileException("Mower Position '$input' does not conform to regular expression '$POSITION_PATTERN'.")
}