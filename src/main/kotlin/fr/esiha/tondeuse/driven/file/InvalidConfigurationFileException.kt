package fr.esiha.tondeuse.driven.file

abstract class InvalidConfigurationFileException(message: String) : Exception(message)