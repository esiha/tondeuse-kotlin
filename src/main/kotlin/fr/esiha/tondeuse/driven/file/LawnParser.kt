package fr.esiha.tondeuse.driven.file

import fr.esiha.tondeuse.domain.Coordinates
import fr.esiha.tondeuse.domain.Lawn
import java.util.regex.Pattern.compile

data class LawnParser(private val input: String) {
    fun parseLawn(): Lawn = with(INPUT_PATTERN.matcher(input)) {
        if (matches()) {
            return Lawn.rectangular(Coordinates(group(ABSCISSA_GROUP).toInt(), group(ORDINATE_GROUP).toInt()))
        }
        throw InvalidLawnDefinitionException(input)
    }

    private companion object {
        private val INPUT_PATTERN = compile("\\s*(\\d+)\\s+(\\d+)\\s*")
        private const val ABSCISSA_GROUP = 1
        private const val ORDINATE_GROUP = 2
    }

    internal data class InvalidLawnDefinitionException(private val input: String) :
            InvalidConfigurationFileException("Lawn definition '$input' does not conform to regular expression '$INPUT_PATTERN'.")
}