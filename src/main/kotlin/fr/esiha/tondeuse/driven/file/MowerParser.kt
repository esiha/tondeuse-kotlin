package fr.esiha.tondeuse.driven.file

import fr.esiha.tondeuse.domain.Mower

data class MowerParser(private val positionParser: MowerPositionParser, private val instructionsParser: MowerInstructionsParser) {
    fun parseMower() = Mower(positionParser.parseMowerPosition(), instructionsParser.parseMowerInstructions())
}
