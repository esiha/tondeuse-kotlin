package fr.esiha.tondeuse.driven.file

import fr.esiha.tondeuse.domain.Configuration

data class ConfigurationParser(private val lawnParser: LawnParser, private val mowerParsers: List<MowerParser>) {
    fun parseConfiguration() = Configuration(
            lawnParser.parseLawn(),
            mowerParsers.map { it.parseMower() }
    )

    companion object {
        operator fun invoke(lines: List<String>) = ConfigurationParser(
                LawnParser(lines.first()),
                (1 until lines.size step 2).map {
                    MowerParser(MowerPositionParser(lines[it]), MowerInstructionsParser(lines[it + 1]))
                }
        )
    }
}
