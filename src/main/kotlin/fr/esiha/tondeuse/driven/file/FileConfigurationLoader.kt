package fr.esiha.tondeuse.driven.file

import fr.esiha.tondeuse.domain.Configuration
import fr.esiha.tondeuse.domain.ConfigurationLoader
import java.io.File

class FileConfigurationLoader(val configurationFile: File) : ConfigurationLoader {
    override fun loadConfiguration(): Configuration = with(configurationFile) {
        if (!exists()) {
            throw NonexistentFileException(this)
        }
        ConfigurationParser(readLines()).parseConfiguration()
    }

    internal data class NonexistentFileException(val file: File) : InvalidConfigurationFileException("The file '$file' does not exist.")
}
