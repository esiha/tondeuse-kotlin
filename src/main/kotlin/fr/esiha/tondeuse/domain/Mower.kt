package fr.esiha.tondeuse.domain

class Mower(val initialPosition: Position, val instructions: List<Instruction>) {
    var currentPosition = initialPosition
        private set

    fun mow(lawn: Lawn) {
        currentPosition = instructions.fold(currentPosition) { position, instruction ->
            with(instruction.execute(position)) {
                if (coordinates in lawn) this else position
            }
        }
    }
}
