package fr.esiha.tondeuse.domain

sealed class Lawn {
    abstract operator fun contains(coordinates: Coordinates): Boolean

    private data class Rectangular(private val topRightCorner: Coordinates) : Lawn() {
        private val bottomLeftCorner = Coordinates.ORIGIN
        override fun contains(coordinates: Coordinates): Boolean =
                coordinates isInUpperRightQuadrantOf bottomLeftCorner
                        && topRightCorner isInUpperRightQuadrantOf coordinates
    }

    companion object {
        fun rectangular(topRightCorner: Coordinates): Lawn = Rectangular(topRightCorner)
    }
}