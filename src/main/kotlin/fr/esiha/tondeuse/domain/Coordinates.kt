package fr.esiha.tondeuse.domain

data class Coordinates constructor(val abscissa: Int, val ordinate: Int) {
    fun plusAbscissa(abscissa: Int) = Coordinates(this.abscissa + abscissa, this.ordinate)
    fun plusOrdinate(ordinate: Int) = Coordinates(this.abscissa, this.ordinate + ordinate)
    infix fun isInUpperRightQuadrantOf(other: Coordinates): Boolean =
            this.abscissa >= other.abscissa && this.ordinate >= other.ordinate

    companion object {
        val ORIGIN = Coordinates(0, 0)
    }
}
