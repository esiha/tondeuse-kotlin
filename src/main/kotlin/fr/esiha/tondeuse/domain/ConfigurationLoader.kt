package fr.esiha.tondeuse.domain

fun interface ConfigurationLoader {
    fun loadConfiguration(): Configuration
}