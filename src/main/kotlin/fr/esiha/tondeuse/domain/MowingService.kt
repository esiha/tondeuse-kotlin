package fr.esiha.tondeuse.domain

interface MowingService {
    fun mow()

    fun currentMowersPositions(): List<Position>
}