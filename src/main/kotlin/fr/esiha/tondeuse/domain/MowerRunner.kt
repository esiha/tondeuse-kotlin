package fr.esiha.tondeuse.domain

import java.lang.IllegalArgumentException
import kotlin.LazyThreadSafetyMode.NONE

class MowerRunner(configurationLoader: ConfigurationLoader) : MowingService {
    private val configuration by lazy { configurationLoader.loadConfiguration() }
    private val lawn by lazy(NONE) { configuration.lawn }
    private val mowers by lazy(NONE) { configuration.mowers }

    override fun mow() {
        assertMowersAreOnTheLawn()
        mowers.forEach { it.mow(lawn) }
    }

    private fun assertMowersAreOnTheLawn() = mowers.forEach {
        if (it.initialPosition.coordinates !in lawn) throw IllegalArgumentException()
    }

    override fun currentMowersPositions(): List<Position> = mowers.map { it.currentPosition }

    companion object {
        operator fun invoke(mowers: List<Mower>, lawn: Lawn) = MowerRunner { Configuration(lawn, mowers) }
    }
}

