package fr.esiha.tondeuse.domain

data class Configuration(val lawn: Lawn, val mowers: List<Mower>)