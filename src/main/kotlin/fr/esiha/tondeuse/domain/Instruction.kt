package fr.esiha.tondeuse.domain

enum class Instruction(val execute: (Position) -> Position) {
    TURN_LEFT(Position::quarterTurnLeft),
    TURN_RIGHT(Position::quarterTurnRight),
    MOVE_ONCE({ it + 1 })
}