package fr.esiha.tondeuse.domain

data class Position(val coordinates: Coordinates, val orientation: Orientation) {
    fun quarterTurnLeft() = copy(orientation = orientation.quarterTurnLeft())
    fun quarterTurnRight() = copy(orientation = orientation.quarterTurnRight())
    operator fun plus(i: Int) = copy(coordinates = orientation.translateCoordinates(coordinates, i))

    companion object {
        operator fun invoke(abscissa: Int, ordinate: Int, orientation: Orientation) = Position(Coordinates(abscissa, ordinate), orientation)
    }
}