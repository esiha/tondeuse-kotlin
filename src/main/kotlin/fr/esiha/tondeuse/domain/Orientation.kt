package fr.esiha.tondeuse.domain

enum class Orientation {
    NORTH {
        override fun quarterTurnLeft() = WEST
        override fun quarterTurnRight() = EAST
        override fun translateCoordinates(coordinates: Coordinates, amount: Int) = coordinates.plusOrdinate(amount)
    },
    SOUTH {
        override fun quarterTurnLeft() = EAST
        override fun quarterTurnRight() = WEST
        override fun translateCoordinates(coordinates: Coordinates, amount: Int) = coordinates.plusOrdinate(-amount)
    },
    EAST {
        override fun quarterTurnLeft() = NORTH
        override fun quarterTurnRight() = SOUTH
        override fun translateCoordinates(coordinates: Coordinates, amount: Int) = coordinates.plusAbscissa(amount)
    },
    WEST {
        override fun quarterTurnLeft() = SOUTH
        override fun quarterTurnRight() = NORTH
        override fun translateCoordinates(coordinates: Coordinates, amount: Int) = coordinates.plusAbscissa(-amount)
    };

    abstract fun quarterTurnLeft(): Orientation
    abstract fun quarterTurnRight(): Orientation
    abstract fun translateCoordinates(coordinates: Coordinates, amount: Int): Coordinates
}