package fr.esiha.tondeuse.driving.cli

import fr.esiha.tondeuse.application.DependenciesFactory.Companion.fileConfigurationLoader
import fr.esiha.tondeuse.application.DependenciesFactory.Companion.mowingService

fun main(args: Array<String>) = with(mowingService(fileConfigurationLoader(args[0]))) {
    mow()
    currentMowersPositions().map { it.toCliPosition() }.forEach { println(it) }
}
