package fr.esiha.tondeuse.driving.cli

import fr.esiha.tondeuse.domain.Position as DomainPosition

internal data class Position(val abscissa: Int, val ordinate: Int, val orientation: Char) {
    override fun toString(): String = "$abscissa $ordinate $orientation"
}

internal fun DomainPosition.toCliPosition(): Position = Position(coordinates.abscissa, coordinates.ordinate, orientation.name[0])